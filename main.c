#include <stdio.h>

int estPalindrome(char* chaine);
int longueur(char* chaine);

int main() {
    char chaine[80];
    printf("Entrez une chaîne: ");
    fgets(chaine, 80, stdin);
    int longueurChaine = longueur(chaine);
    printf("La chaîne fait %d caractères\n", longueurChaine);

    if (estPalindrome(chaine) == 1) {
        printf("La chaîne est un palindrome\n");
    } else {
        printf("La chaîne n'est pas un palindrome\n");
    }
}

int longueur(char* chaine) {
    int i = 0;
    while ( chaine[i] != '\0' && chaine[i] != '\n' ) {
        i++;
    }
    return i;
}

int estPalindrome(char* chaine) {
    int i = longueur(chaine);
    int j = 0;
    while ( j < i/2 ) {
        if ( chaine[j] != chaine[i-j-1] ) {
            return 0;
        }
        j++;
    }
    return 1;
}
